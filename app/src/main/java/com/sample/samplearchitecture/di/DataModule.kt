package com.sample.samplearchitecture.di

import com.sample.samplearchitecture.data.UserRepository
import com.sample.samplearchitecture.data.UserRepositoryData
import com.sample.samplearchitecture.data.remote.UserDataSource
import com.sample.samplearchitecture.data.remote.UserRemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class DataModule {

    @Binds
    abstract fun provideUserRepository(repository: UserRepositoryData): UserRepository


    @Binds
    abstract fun provideUserDataSource(datasource: UserRemoteDataSource): UserDataSource
}
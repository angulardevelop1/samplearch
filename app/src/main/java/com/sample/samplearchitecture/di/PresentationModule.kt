package com.sample.samplearchitecture.di

import com.sample.samplearchitecture.presentation.main.MainContract
import com.sample.samplearchitecture.presentation.main.MainPresenter
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
abstract class PresentationModule {

    @Binds
    abstract fun provideMainPresenter(presenter: MainPresenter): MainContract.Presenter
}
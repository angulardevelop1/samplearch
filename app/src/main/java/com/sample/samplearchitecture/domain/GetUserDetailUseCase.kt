package com.sample.samplearchitecture.domain

import io.reactivex.Single
import javax.inject.Inject

class GetUserDetailUseCase @Inject constructor() : SingleUseCase<String, Unit?> {
    override fun buildObservable(params: Unit?): Single<String> {
        return Single.just("HOLA ESTA ES LA INFO DEL USUARIO")
        //.delay(4500, TimeUnit.MILLISECONDS)
    }
}
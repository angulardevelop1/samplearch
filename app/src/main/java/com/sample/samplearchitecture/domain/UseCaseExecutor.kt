package com.sample.samplearchitecture.domain

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

interface UseCaseExecutor {

    operator fun <T, P> SingleUseCase<T, P>.invoke(
        params: P,
        consumerSuccess: Consumer<T>,
        consumerError: Consumer<Throwable>,
        backScheduler: Scheduler = Schedulers.io(),
        uiScheduler: Scheduler = AndroidSchedulers.mainThread()
    )

    operator fun <O, P> SingleUseCase<O, P>.invoke(
        params: ExecuteParams<O, P>
    )

    fun <O, P> SingleUseCase<O, P>.executeWith(
        params: ExecuteParams<O, P>
    )

    fun stop()
}

class DefaultUseCaseExecutor : UseCaseExecutor {
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override operator fun <T, P> SingleUseCase<T, P>.invoke(
        params: P,
        consumerSuccess: Consumer<T>,
        consumerError: Consumer<Throwable>,
        backScheduler: Scheduler,
        uiScheduler: Scheduler
    ) {
        Timber.e("execute ...... ")
        compositeDisposable.add(
            buildObservable(params)
                .subscribeOn(backScheduler)
                .observeOn(uiScheduler)
                .subscribe(consumerSuccess, consumerError)
        )
    }

    override fun <T, P> SingleUseCase<T, P>.invoke(params: ExecuteParams<T, P>) {
        Timber.e("execute ...... ")
        compositeDisposable.add(
            buildObservable(params.params)
                .subscribeOn(params.backScheduler)
                .observeOn(params.uiScheduler)
                .subscribe(params.consumerSuccess, params.consumerError)
        )
    }

    override fun <O, P> SingleUseCase<O, P>.executeWith(params: ExecuteParams<O, P>) {
        Timber.e("execute ...... ")
        compositeDisposable.add(
            buildObservable(params.params)
                .subscribeOn(params.backScheduler)
                .observeOn(params.uiScheduler)
                .subscribe(params.consumerSuccess, params.consumerError)
        )
    }

    override fun stop() {
        if (!compositeDisposable.isDisposed) {
            Timber.e("disposing observable...")
            compositeDisposable.dispose()
        }
    }
}

class ExecuteParams<T, P> private constructor(
    val params: P,
    val consumerSuccess: Consumer<T>,
    val consumerError: Consumer<Throwable>,
    val backScheduler: Scheduler = Schedulers.io(),
    val uiScheduler: Scheduler = AndroidSchedulers.mainThread()
) {
    private constructor(builder: Builder<T, P>) : this(
        builder.params,
        builder.consumerSuccess,
        builder.consumerError,
        builder.backScheduler,
        builder.uiScheduler
    )

    data class Builder<OUTPUT, PARAMS>(val params: PARAMS) {
        var consumerSuccess: Consumer<OUTPUT> = Consumer { }
            private set
        var consumerError: Consumer<Throwable> = Consumer { }
            private set
        var backScheduler: Scheduler = Schedulers.io()
            private set
        var uiScheduler: Scheduler = AndroidSchedulers.mainThread()
            private set

        fun onSuccess(success: Consumer<OUTPUT>): Builder<OUTPUT, PARAMS> {
            this.consumerSuccess = success
            return this
        }

        fun onError(error: Consumer<Throwable>): Builder<OUTPUT, PARAMS> {
            this.consumerError = error
            return this
        }

        fun build(): ExecuteParams<OUTPUT, PARAMS> {
            return ExecuteParams(this)
        }

    }
}
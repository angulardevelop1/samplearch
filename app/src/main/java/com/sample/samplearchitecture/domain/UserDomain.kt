package com.sample.samplearchitecture.domain

data class UserDomain(
    val id: Int,
    val name: String,
    val email: String
)
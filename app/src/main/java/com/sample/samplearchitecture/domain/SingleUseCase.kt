package com.sample.samplearchitecture.domain

import io.reactivex.Single

interface SingleUseCase<OUTPUT, in PARAMS> {

    fun buildObservable(params: PARAMS): Single<OUTPUT>
}
package com.sample.samplearchitecture.domain

import com.sample.samplearchitecture.data.UserRepository
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class GetUsersUseCase @Inject constructor(
    private val repository: UserRepository
) : SingleUseCase<List<UserDomain>, Unit?> {

    override fun buildObservable(params: Unit?): Single<List<UserDomain>> {
        Timber.d(javaClass.canonicalName)
        return repository.getUsers()
    }
}
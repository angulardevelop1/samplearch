package com.sample.samplearchitecture.presentation.commons;

public interface BaseContract {


    interface BaseView {
    }

    interface BasePresenter<T extends BaseView> {
        void bindView(T view);

        void unbindView();

        T getView();
    }
}
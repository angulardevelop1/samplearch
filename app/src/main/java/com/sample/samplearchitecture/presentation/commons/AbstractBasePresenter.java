package com.sample.samplearchitecture.presentation.commons;

import java.lang.ref.WeakReference;

public abstract class AbstractBasePresenter<T extends BaseContract.BaseView> implements BaseContract.BasePresenter<T> {
    private WeakReference<T> view;

    @Override
    public void bindView(T view) {
        this.view = new WeakReference<T>(view);
    }

    @Override
    public void unbindView() {
        this.view = null;
    }

    @Override
    public T getView() {
        if (this.view == null) return null;
        return this.view.get();
    }
}
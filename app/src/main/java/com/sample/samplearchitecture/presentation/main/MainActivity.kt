package com.sample.samplearchitecture.presentation.main

import android.os.Bundle
import android.view.View
import android.widget.Button
import com.sample.samplearchitecture.R
import com.sample.samplearchitecture.databinding.ActivityMainBinding
import com.sample.samplearchitecture.domain.UserDomain
import com.sample.samplearchitecture.presentation.commons.BaseMvpActivity
import com.sample.samplearchitecture.presentation.detail.DetailActivity
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseMvpActivity<MainContract.Presenter>(), MainContract.View {
    private lateinit var binding: ActivityMainBinding
    private val options: List<Pair<Button, Runnable>> by lazy {
        listOf(
            Pair(binding.btnLoad1, Runnable { presenter.loadUsersFunctionalParams() }),
            Pair(binding.btnLoad2, Runnable { presenter.loadUsersFunctionalExecuteParams() }),
            Pair(binding.btnLoad3, Runnable { presenter.loadUsersBuilderExecuteParams() }),
            Pair(binding.btnLoad4, Runnable { presenter.loadUsersTradicional() })
        )
    }

    @Inject
    lateinit var stringInjected: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupViews()
        presenter.initView()
    }

    private fun setupViews() {
        if (this::stringInjected.isInitialized)
            binding.tvHello.text = stringInjected
        binding.btnShowDetails.setOnClickListener {
            startActivity(DetailActivity.getIntent(this))
            finish()
        }
        options.forEach { it.first.assignClearClickListener { it.second.run() } }
    }

    private fun Button.assignClearClickListener(listener: () -> Unit) {
        this.setOnClickListener {
            cleanAndShowLoading()
            listener.invoke()
        }
    }

    private fun cleanAndShowLoading() {
        binding.tvHello.text = ""
        binding.pbLoading.visibility = View.VISIBLE
    }

    override fun showData(list: List<UserDomain>) {
        Timber.e("llego al data")
        binding.pbLoading.visibility = View.GONE
        val stringBuilder = StringBuilder()
        list.forEach { user -> stringBuilder.appendLine(user.name) }
        binding.tvHello.text = stringBuilder
    }

    override fun showError() {
        Timber.e("llego al error")
        binding.pbLoading.visibility = View.GONE
        binding.tvHello.text = getString(R.string.error_message)
    }
}
package com.sample.samplearchitecture.presentation.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sample.samplearchitecture.R

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
    }

    companion object {
        fun getIntent(context: Context): Intent {
            return Intent(context, DetailActivity::class.java).apply {
                putExtra("test", "test extra")
            }
        }
    }
}
package com.sample.samplearchitecture.presentation.main

import com.sample.samplearchitecture.domain.UserDomain
import com.sample.samplearchitecture.presentation.commons.BaseContract.BasePresenter
import com.sample.samplearchitecture.presentation.commons.BaseContract.BaseView

interface MainContract {

    interface View : BaseView {
        fun showData(list: List<UserDomain>)
        fun showError()
    }

    interface Presenter : BasePresenter<View> {
        fun initView()
        fun loadUsersFunctionalParams()
        fun loadUsersFunctionalExecuteParams()
        fun loadUsersBuilderExecuteParams()
        fun loadUsersTradicional()
    }
}
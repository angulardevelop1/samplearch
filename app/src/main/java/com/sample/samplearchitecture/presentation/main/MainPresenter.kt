package com.sample.samplearchitecture.presentation.main

import com.sample.samplearchitecture.domain.*
import com.sample.samplearchitecture.presentation.commons.AbstractBasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

/*
* Si tengo un caso de uso debe hacer el dispose cuado la vista ya no existe. (true)
* Si tengo mas de un caso de uso de igual manera manejar el dispose. (true)
* */
class MainPresenter @Inject constructor(
    private val getUsersUseCase: GetUsersUseCase,
    private val getUserDetailUseCase: GetUserDetailUseCase
) : AbstractBasePresenter<MainContract.View>(), MainContract.Presenter,
    UseCaseExecutor by DefaultUseCaseExecutor() {

    //TODO: only loadUsersTradicional
    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    override fun initView() {
        loadUsersFunctionalParams()
        getUserDetailUseCase(
            params = Unit,
            consumerSuccess = {
                Timber.d("show data")
            },
            consumerError = {
                Timber.d("error")
            }
        )
    }

    // TODO: Recommended
    override fun loadUsersFunctionalParams() {
        getUsersUseCase(
            params = Unit,
            consumerSuccess = { users ->
                view.showData(users)
            },
            consumerError = {
                view.showError()
            }
        )
    }

    // TODO: Recommended
    override fun loadUsersFunctionalExecuteParams() {
        getUsersUseCase(ExecuteParams.Builder<List<UserDomain>, Unit>(Unit)
            .onSuccess { view.showData(it) }
            .onError { view.showError() }
            .build())
    }

    // TODO: Recommended
    override fun loadUsersBuilderExecuteParams() {
        val executeParams = ExecuteParams.Builder<List<UserDomain>, Unit>(Unit)
            .onSuccess {
                view.showData(it)
            }
            .onError {
                view.showError()
            }.build()
        getUsersUseCase.executeWith(executeParams)
    }

    // TODO: NOT Recommended
    override fun loadUsersTradicional() {
        val disposable = getUsersUseCase.buildObservable(Unit)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ users ->
                Timber.d("response success: $users")
                view.showData(users)
            }, {
                view.showError()
            })
        this.compositeDisposable.add(disposable)
    }

    override fun unbindView() {
        this.stop()
        manageDisposable()
        super.unbindView()
    }

    // TODO: only loadUsersTradicional
    private fun manageDisposable() {
        if (!compositeDisposable.isDisposed) {
            Timber.e("disposing observable...")
            compositeDisposable.dispose()
        }
    }
}
package com.sample.samplearchitecture.data.remote.model

import com.google.gson.annotations.SerializedName
import com.sample.samplearchitecture.domain.UserDomain

data class UserModel(
    @SerializedName("userId")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("email")
    val email: String
)

fun List<UserModel>.toListDomain() = this.map { model -> model.toDomain() }
fun UserModel.toDomain() = UserDomain(
    id = id,
    name = name,
    email = email
)
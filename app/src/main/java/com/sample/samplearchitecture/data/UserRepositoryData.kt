package com.sample.samplearchitecture.data

import com.sample.samplearchitecture.data.remote.UserDataSource
import com.sample.samplearchitecture.domain.UserDomain
import io.reactivex.Single
import javax.inject.Inject

class UserRepositoryData @Inject constructor(
    private val dataSource: UserDataSource
) : UserRepository {

    override fun getUsers(): Single<List<UserDomain>> {
        return dataSource.getUsers()
    }
}
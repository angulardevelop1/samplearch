package com.sample.samplearchitecture.data.remote

import com.sample.samplearchitecture.domain.UserDomain
import io.reactivex.Single

interface UserDataSource {

    fun getUsers(): Single<List<UserDomain>>

}
package com.sample.samplearchitecture.data.remote

import com.sample.samplearchitecture.data.SampleApi
import com.sample.samplearchitecture.data.remote.model.toListDomain
import com.sample.samplearchitecture.domain.UserDomain
import io.reactivex.Single
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class UserRemoteDataSource @Inject constructor(val api: SampleApi) : UserDataSource {

    override fun getUsers(): Single<List<UserDomain>> {
        Timber.d("call datasource")
        return api.getUsers()
            .map { it.toListDomain() }
            .delay(1500, TimeUnit.MILLISECONDS)
    }
}
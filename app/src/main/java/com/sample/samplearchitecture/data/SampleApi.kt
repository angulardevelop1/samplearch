package com.sample.samplearchitecture.data

import com.sample.samplearchitecture.data.remote.model.UserModel
import io.reactivex.Single
import retrofit2.http.GET

interface SampleApi {

    @GET("/users")
    fun getUsers(): Single<List<UserModel>>
}
package com.sample.samplearchitecture.data

import com.sample.samplearchitecture.domain.UserDomain
import io.reactivex.Single

interface UserRepository {

    fun getUsers(): Single<List<UserDomain>>
}